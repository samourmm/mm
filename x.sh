#!/bin/sh
##############################
####### Mohammad Samour ######
##############################

# fail on any error or unset variable
#set -e -u -o pipefail

# get user name
read -p "Please enter your bitbucket username: " username

read -p "Please enter your bitbucket project key: " project

# -s flag hides password text
read -s -p "Please enter your bitbucket password: " password


echo ""

# get repo name
dir_name=`basename $(pwd)`
read -p "Do you want to use '$dir_name' as a repo name?(y/n)" answer_dirname
case $answer_dirname in
  y)
    # use currently dir name as a repo name
    reponame=$dir_name
    ;;
  n)
    read -p "Enter your new repository name: " reponame
    if [ "$reponame" = "" ]; then
        reponame=$dir_name
    fi
    ;;
  *)
    ;;
esac

# Make API call to create the remote repo on bitbucket
curl -sS --location --fail -i -X POST \
	-u "${username}:${password}" \
	-H "Content-Type: application/json" \
	-d "{ \"scm\": \"git\", \"is_private\": \"$PRIVATE\"}" \
	"https://api.bitbucket.org/2.0/repositories/${username}/{$reponame}"

#check exit status
if [[ $? -eq 0 ]];then
echo "repo was created successfully!"
else
echo "error creating the repo"
fi

# create empty README.md
echo "Creating README ..."
touch README.md

# push to remote repo
echo "Pushing to remote ..."
git init
git add -A
git commit -m "Initial commit"
git remote rm origin
git remote add origin https://$username@bitbucket.org/$username/$reponame.git
git push -u origin master

git checkout master
git pull
git checkout -b dev
git add -A
git commit -m "uat branch"
git push -u origin dev 

# open in a browser
read -p "Do you want to open the new repo page in browser?(y/n): " answer_browser

case $answer_browser in
  y)
    echo "Opening in a browser ..."
    open https://bitbucket.org/$username/$reponame
    ;;
  n)
    ;;
  *)
    ;;
esac

bash-3.2$ 

